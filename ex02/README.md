# Ex02

## Goal
learn some cool commands

## commands
`mkdir` - create a folder
`rm -r` - remove a folder 
`touch` - create an empty file
`clear` - clear terminal screen
`echo` - write to stdout
`grep -irn palavra` - search recursively for word palavra in files
`hostname -f` - check machine hostname
`ifconfig` - check internet interfaces
`netstat -tulpn` - check LISTEN ports - might need to install net-tools
`ps aux` - list processes
`top` - list processes in a top way
`ssh` - perform secure shell

