# Ex01

## Goal
find the file with "olá mundo" content 

## Commands

`ls` to list
`cat file.txt` to view file content
`cd folder` to change directory to folder
`cd ..` to go back one directory
